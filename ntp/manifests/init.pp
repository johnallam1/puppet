class ntp {
	$vartemp1='testing'
	package {'ntp':
	ensure => 'installed',
	   	}
	$a=ntp_version_only(ntp)
	notice ($a)

	service {'ntpd':
	ensure => 'running',
	require => Package['ntp'],
		}
	file {'/tmp/target_file':
	ensure => 'present',
	owner => 'ec2-user',
	group => 'root',
	mode => '644',
	source => 'puppet:///modules/ntp/test_file',
	      }
	file {'/tmp/template_targetfile':
	ensure => 'present',
	owner => 'ec2-user',
	group => 'root',		
	mode => '644',
	content => template('ntp/test_temp.erb'),
	    }
	}
